# PDF-Squeeze-shellscript - Version 1.2.0c

/*
* PDF-Squeeze - A script to make it easy to optimize/compress PDF files.
*
* THIS IMPROVED VERSION in Shell-script,
* Copyright (c) 2022, Jim S. Smith, AFWS
* All rights reserved.
*
* https:// -
* Licensed under GPL 2.0 or newer.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*
* This version was inspired by an example called 'shrinkpdf' similar to it at:
*    https://bash.cyberciti.biz/file-management/linux-shell-script-to-reduce-pdf-file-size/
*    Published by: Vivek Gite - "nixCraft" blog, DTD: November 10, 2021.
*
* And its original version of 'shrinkpdf' from: Alfred Klomp, Copyright (c) 2014-2019, Alfred Klomp
*    Found at: http://www.alfredklomp.com/programming/shrinkpdf/
*
* REQUIREMENTS:
*
*    Ghostscript utility/library,
*    Linux CLI mode,
*
*    This new version adds the ability to automatically detect what version of PDF the source is,
*    And - adds the option to convert colors into gray-scale.
*
*/
